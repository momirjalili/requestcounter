# Instructions

to run simple run the following commands:

```bash
docker-compose build
docker-compose up
```

the service will be available on port 80 of your localhost
 
# Details

the project is written using python cherrypy framework.
since cherrypy provides a production level http server,
using external servers for hosting wsgi application
(e.g. uwsgi, gunicorn) and a web server (e.g. nginx) is avoided.
the only dependency to run the application is cherrypy.

# Tests
to run the tests you must build the project in development 
mode. you can use the following commands

```bash
docker-compose -f dev.yml build
docker-compose -f dev.yml run cherrypy py.test -s app/counter_test/__init__.py
```

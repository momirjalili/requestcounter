import time
import shelve

import cherrypy

SHELVE_PATH = '/server_data/shelf.db'


class Counter(object):

    def __init__(self):
        with shelve.open(SHELVE_PATH, writeback=True) as db:
            if not db.get('last_minute_count'):
                db['last_minute_count'] = [(0, 0) for i in range(60)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self):
        now = time.time()
        index = int(now % 60)

        with shelve.open(SHELVE_PATH, writeback=True) as db:
            old_time, hit = db['last_minute_count'][index]
            if now - old_time < 1:
                db['last_minute_count'][index] = now, hit + 1
            else:
                db['last_minute_count'][index] = now, 1
            last_minute_count = db['last_minute_count']

        return {
            "countOfRequests": self.get_hits(now, last_minute_count),
        }

    def get_hits(self, now, last_minute_count):
        hits = 0
        for time_hit, hit in last_minute_count:
            if now - time_hit < 60:
                hits += hit
        return hits


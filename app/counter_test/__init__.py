import cherrypy
from cherrypy.test import helper
import pytest
from ..counter import Counter
import time


@pytest.fixture(params=[(1, 60, True), (0, 0, True), (1, 0, False), (0, 0, False)])
def last_minute_count(request):
    now = time.time()
    if request.param[2]:
        req_time = now
    else:
        req_time = now - 61

    last_minute_count_list = [(req_time, request.param[0]) for i in range(60)]
    expected_count = request.param[1]
    return (now, last_minute_count_list, expected_count)


def test_get_hits(last_minute_count):
    counter = Counter()
    assert counter.get_hits(last_minute_count[0],
                            last_minute_count[1]) == last_minute_count[2]
